link project bisa diakses di [SSH Web Console](https://gitlab.com/alazimdev/ssh-web-console).

# Instalasi

Tahapan instalasi:

## Server

```terminal
cd ./server
npm install
npm start
```


## Client

```terminal
cd ./client
npm install
npm start
```